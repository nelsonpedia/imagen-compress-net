﻿using ImagenCompresion.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TinyPng;

namespace ImagenCompresion.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> SubirImagen(HttpPostedFileBase Imagen)
        {
            try
            {
                if (Imagen.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(Imagen.FileName); // nombre de la imagen
                    string _path = Path.Combine(Server.MapPath("~/Comprimidas"), _FileName); // conbinar la ruta de la ruta contenedora con el nombre de archivo

                    // Image.SaveAs(_path);
                    using (var png = new TinyPngClient("ZbLm1HXwYfKZfxYb6cW2ThprYWM3NT2L"))
                    {

                        await png.Compress(_path)
                            .Download()
                            .SaveImageToDisk(Path.Combine(Server.MapPath("~/Comprimidas"), "comprimida.jpg"));
                    }

                }
                return Redirect("Index");
            }
            catch(Exception ex) 
            {
                ViewBag.Message = "File upload failed!!";
                return View();
            }
        }
    }
}