﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ImagenCompresion.Models.ViewModels
{
    public class ImagenVM
    {
        public string Imagen { get; set; }
    }
}